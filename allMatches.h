#ifndef _MASH_ALL_MATCHES_H_
#define _MASH_ALL_MATCHES_H_

#include <iostream>
#include <vector>

std::vector<std::vector<double>> allMatches(std::vector<std::string> filenames);

#endif
