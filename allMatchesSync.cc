#include <iostream>
#include <vector>
#include <nan.h>
#include "allMatchesSync.h"
#include "allMatches.h"

using namespace::std;
using namespace v8;
using namespace Nan;

// Simple synchronous access to the `allMatches()` function
NAN_METHOD(AllMatchesSync) {
  vector<string> filenames;
  for (unsigned int i = 0; i < 2; i++) {
    v8::String::Utf8Value v8String(info[i]->ToString());
    string stlString = std::string(*v8String);
    filenames.push_back(stlString);
  }

  vector<vector<double>> matches = allMatches(filenames);

  Local<Array> results = New<Array>();

  for (uint64_t k = 0; k < matches.size(); k++)
  {
    Local<Array> r = New<Array>();
    r->Set(0, New<Number>(matches[k][0]));
    r->Set(1, New<Number>(matches[k][1]));
    r->Set(2, New<Number>(matches[k][2]));
    r->Set(3, New<Number>(matches[k][3]));
    r->Set(4, New<Number>(matches[k][4]));

    results->Set(k, r);
  }

  info.GetReturnValue().Set(results);
}
