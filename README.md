# mash-node-native
Native Node.js module for Mash (https://github.com/marbl/Mash)

# Installation

## Mac OS X

### Autoconf (http://www.gnu.org/software/autoconf/autoconf.html)

```
curl -sL https://gist.github.com/jellybeansoup/4192307/raw/c6bd873d53afba6cc3d3528a3cfc0dbf683f337f/cltools.sh | bash -
```


### Cap’n Proto (https://capnproto.org/install.html)

Same as Linux (see below).


### GSL - GNU Scientific Library (http://www.gnu.org/software/gsl/)

```
sudo brew install gsl
```


### Mash (https://gitlab.com/cgps/mash/blob/master/INSTALL.txt)

Same as Linux (see below).


## Linux

### Autoconf (http://www.gnu.org/software/autoconf/autoconf.html)

```
sudo apt-get update
sudo apt-get install autoconf
```

### Cap’n Proto (https://capnproto.org/install.html)

```
curl -O https://capnproto.org/capnproto-c++-0.5.1.2.tar.gz
tar zxf capnproto-c++-0.5.1.2.tar.gz
cd capnproto-c++-0.5.1.2
./configure --enable-shared CXXFLAGS="-fPIC -fpermissive"
make -j6 check
sudo make install
```


### GSL - GNU Scientific Library (http://www.gnu.org/software/gsl/)

```
sudo apt-get update
sudo apt-get install gsl-bin libgsl0-dev
```


### Zlib

```
sudo apt-get update
sudo apt-get install -y zlib1g-dev
```


### Mash (https://gitlab.com/cgps/mash/blob/master/INSTALL.txt)

```
git clone https://gitlab.com/cgps/mash-original.git
cd mash
./bootstrap.sh
./configure --enable-shared CXXFLAGS=-fPIC
CPPFLAGS="-fPIC" make
sudo make install
```
