#!/usr/bin/env bash
set -e

registry="registry.gitlab.com/cgps/mash-node-native"
version=`yaml get package.json version`

docker build \
  -f ./docker/Dockerfile \
  -t $registry:$version \
  .
