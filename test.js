const path = require('path');
const mash = require('./index');

const ref = 'test-data/refseq-all-k16-s400.msh';
const fasta = 'test-data/MRSA08.fa';

try {
  const match = mash.closestMatchSync(ref, fasta);
  console.log(match);
} catch (e) {
  console.error('ERROR', e);
}

mash.closestMatch(ref, fasta)
  .then(results => {
    console.log(results);
  })
  .catch(error => {
    console.error(error);
  });

try {
  const matches = mash.allMatchesSync(ref, fasta);
  console.dir(matches);
} catch (e) {
  console.error('ERROR', e);
}

console.log('Done');
