#include "CommandDistance.h"
#include "Sketch.h"
#include <iostream>
#include <zlib.h>
#include "ThreadPool.h"
#include "sketchParameterSetup.h"
#include <math.h>

#ifdef USE_BOOST
  #include <boost/math/distributions/binomial.hpp>
  using namespace::boost::math;
#else
  #include <gsl/gsl_cdf.h>
#endif

#include "CustomCommandDistance.h"

using namespace::std;

CustomCommandDistance::CustomCommandDistance()
: CommandDistance()
{
}

std::vector<CommandDistance::CompareOutput::PairOutput> CustomCommandDistance::getAllMatches(std::vector<string> filenames) const
{
  std::vector<const char*> cstrings{};

  for (auto& string : filenames)
  {
    cstrings.push_back(&string.front());
  }

  ((Command *)this)->run(cstrings.size(), cstrings.data());

  return this->outputPairs;
}

int CustomCommandDistance::run() const
{
  if ( arguments.size() < 2 || options.at("help").active )
  {
  print();
  return 0;
  }

  int threads = options.at("threads").getArgumentAsNumber();
  bool list = options.at("list").active;
  bool table = options.at("table").active;
  //bool log = options.at("log").active;
  double pValueMax = options.at("pvalue").getArgumentAsNumber();
  double distanceMax = options.at("distance").getArgumentAsNumber();

  Sketch::Parameters parameters;

  if ( sketchParameterSetup(parameters, *(Command *)this) )
  {
  	return 1;
  }

  Sketch sketchRef;

  uint64_t lengthMax;
  double randomChance;
  int kMin;
  string lengthMaxName;
  int warningCount = 0;

  const string & fileReference = arguments[0];

  bool isSketch = hasSuffix(fileReference, suffixSketch);

  if ( isSketch )
  {
  if ( options.at("kmer").active )
  {
    cerr << "ERROR: The option -" << options.at("kmer").identifier << " cannot be used when a sketch is provided; it is inherited from the sketch." << endl;
    return 1;
  }

  if ( options.at("noncanonical").active )
  {
    cerr << "ERROR: The option -" << options.at("noncanonical").identifier << " cannot be used when a sketch is provided; it is inherited from the sketch." << endl;
    return 1;
  }

  if ( options.at("protein").active )
  {
    cerr << "ERROR: The option -" << options.at("protein").identifier << " cannot be used when a sketch is provided; it is inherited from the sketch." << endl;
    return 1;
  }

  if ( options.at("alphabet").active )
  {
    cerr << "ERROR: The option -" << options.at("alphabet").identifier << " cannot be used when a sketch is provided; it is inherited from the sketch." << endl;
    return 1;
  }
  }
  else
  {
  cerr << "Sketching " << fileReference << " (provide sketch file made with \"mash sketch\" to skip)...";
  }

  vector<string> refArgVector;
  refArgVector.push_back(fileReference);

  //cerr << "Sketch for " << fileReference << " not found or out of date; creating..." << endl;

  sketchRef.initFromFiles(refArgVector, parameters);

  double lengthThreshold = (parameters.warning * sketchRef.getKmerSpace()) / (1. - parameters.warning);

  if ( isSketch )
  {
  if ( options.at("sketchSize").active )
  {
    if ( parameters.reads && parameters.minHashesPerWindow != sketchRef.getMinHashesPerWindow() )
    {
    cerr << "ERROR: The sketch size must match the reference when using a bloom filter (leave this option out to inherit from the reference sketch)." << endl;
    return 1;
    }
  }

  parameters.minHashesPerWindow = sketchRef.getMinHashesPerWindow();
  parameters.kmerSize = sketchRef.getKmerSize();
  parameters.noncanonical = sketchRef.getNoncanonical();
  parameters.preserveCase = sketchRef.getPreserveCase();

  string alphabet;
  sketchRef.getAlphabetAsString(alphabet);
  setAlphabetFromString(parameters, alphabet.c_str());
  }
  else
  {
  for ( uint64_t i = 0; i < sketchRef.getReferenceCount(); i++ )
  {
    uint64_t length = sketchRef.getReference(i).length;

    if ( length > lengthThreshold )
    {
    if ( warningCount == 0 || length > lengthMax )
    {
      lengthMax = length;
      lengthMaxName = sketchRef.getReference(i).name;
      randomChance = sketchRef.getRandomKmerChance(i);
      kMin = sketchRef.getMinKmerSize(i);
    }

    warningCount++;
    }
  }

  cerr << "done.\n";
  }

  if ( table )
  {
  cout << "#query";

  for ( int i = 0; i < sketchRef.getReferenceCount(); i++ )
  {
    cout << '\t'
      << sketchRef.getReference(i).name;
  }

  cout << endl;
  }

  ThreadPool<CompareInput, CompareOutput> threadPool(compare, threads);

  vector<string> queryFiles;

  for ( int i = 1; i < arguments.size(); i++ )
  {
    if ( list )
    {
      splitFile(arguments[i], queryFiles);
    }
    else
    {
      queryFiles.push_back(arguments[i]);
    }
  }

  Sketch sketchQuery;

  sketchQuery.initFromFiles(queryFiles, parameters, 0, true);

  // Sketch::SketchOutput * output = sketchFile(new Sketch::SketchInput(queryFiles[0], 0, 0, "", "", parameters));
  // sketchQuery.useThreadOutput(output);
  // vector<string> files;
  // sketchQuery.initFromFiles(files, parameters, 0, true);

  uint64_t pairCount = sketchRef.getReferenceCount() * sketchQuery.getReferenceCount();
  uint64_t pairsPerThread = pairCount / parameters.parallelism;

  if ( pairsPerThread == 0 )
  {
  	pairsPerThread = 1;
  }

  static uint64_t maxPairsPerThread = 0x1000;

  if ( pairsPerThread > maxPairsPerThread )
  {
  pairsPerThread = maxPairsPerThread;
  }

  uint64_t iFloor = pairsPerThread / sketchRef.getReferenceCount();
  uint64_t iMod = pairsPerThread % sketchRef.getReferenceCount();

  for ( uint64_t i = 0, j = 0; i < sketchQuery.getReferenceCount(); i += iFloor, j += iMod )
  {
    if ( j >= sketchRef.getReferenceCount() )
    {
      if ( i == sketchQuery.getReferenceCount() - 1 )
      {
      break;
      }

      i++;
      j -= sketchRef.getReferenceCount();
    }

    threadPool.runWhenThreadAvailable(new CompareInput(sketchRef, sketchQuery, j, i, pairsPerThread, parameters, distanceMax, pValueMax));

    while ( threadPool.outputAvailable() )
    {
      writeCustomOutput(threadPool.popOutputWhenAvailable(), table);
    }
  }

  while ( threadPool.running() )
  {
    writeCustomOutput(threadPool.popOutputWhenAvailable(), table);
  }

  if ( warningCount > 0 && ! parameters.reads )
  {
  	warnKmerSize(parameters, *this, lengthMax, lengthMaxName, randomChance, kMin, warningCount);
  }

  return 0;
}

void CustomCommandDistance::writeCustomOutput(CompareOutput * output, bool table) const
{
  // pairs = output->pairs;
  // pairCount = output->pairCount;
  //
  // std::vector<double> outputResult;

  uint64_t i = output->indexQuery;
  uint64_t j = output->indexRef;

  for ( uint64_t k = 0; k < output->pairCount && i < output->sketchQuery.getReferenceCount(); k++ )
  {
    const CompareOutput::PairOutput * pair = &output->pairs[k];

    if ( table && j == 0 )
    {
      cout << output->sketchQuery.getReference(i).name;
    }

    if ( table )
    {
      cout << '\t';

      if ( pair->pass )
      {
        cout << pair->distance;
      }
    }
    else if ( pair->pass )
    {
      // cout << output->sketchRef.getReference(j).name << '\t'
      //   << output->sketchQuery.getReference(i).name << '\t'
      //   << pair->distance << '\t'
      //   << pair->pValue << '\t'
      //   << pair->numer << '/' << pair->denom << endl;

      // outputResult.push_back(pair->distance);
      // outputResult.push_back(pair->pValue);
      // outputResult.push_back(pair->numer);
      // outputResult.push_back(pair->denom);

      outputPairs.push_back(output->pairs[k]);
    }

    j++;

    if ( j == output->sketchRef.getReferenceCount() )
    {
      if ( table )
      {
        cout << endl;
      }

      j = 0;
      i++;
    }
  }

  delete output;
}
