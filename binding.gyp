{
  "targets": [
    {
      "target_name": "addon",
      "sources": [
        "addon.cc",
        "sync.cc",
        "async.cc",
        "closestMatch.cc",
        "CustomCommandDistance.cpp",
        "allMatches.cc",
        "allMatchesSync.cc"
      ],
      "cflags": [
        "-Wall",
        "-std=c++11"
      ],
      "include_dirs": [
        ".",
        "<!(node -e \"require('mash-original')\")",
        "<!(node -e \"require('nan')\")",
        "/usr/local/include"
      ],
      "libraries": [
        "/usr/local//lib/libmash.a",
        "/usr/local//lib/libcapnp.a",
        "/usr/local//lib/libkj.a",
        "-L/usr/local//lib",
        "-lgsl",
        "-lgslcblas",
        "-lstdc++",
        "-lz",
        "-lm",
        "-lpthread"
      ],
      "conditions": [
        [
          'OS=="mac"',
          {
            "xcode_settings": {
              'OTHER_CPLUSPLUSFLAGS' : [ '-std=c++11','-stdlib=libc++' ],
              'OTHER_LDFLAGS': [ '-stdlib=libc++' ],
              'MACOSX_DEPLOYMENT_TARGET': '10.10'
            }
          }
        ]
      ]
    }
  ]
}
