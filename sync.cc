#include <iostream>
#include <vector>
#include <nan.h>
#include "sync.h"
#include "closestMatch.h"

using namespace::std;
using namespace v8;
using namespace Nan;

// Simple synchronous access to the `closestMatch()` function
NAN_METHOD(ClosestMatchSync) {
  vector<string> filenames;
  for (unsigned int i = 0; i < 2; i++) {
    v8::String::Utf8Value v8String(info[i]->ToString());
    string stlString = std::string(*v8String);
    filenames.push_back(stlString);
  }

  vector<double> match = closestMatch(filenames);

  Local<Array> results = New<Array>();
  results->Set(0, New<Number>(match[0]));
  results->Set(1, New<Number>(match[1]));
  results->Set(2, New<Number>(match[2]));
  results->Set(3, New<Number>(match[3]));
  results->Set(4, New<Number>(match[4]));

  info.GetReturnValue().Set(results);
}
