curl -O https://capnproto.org/capnproto-c++-0.5.1.2.tar.gz
tar zxf capnproto-c++-0.5.1.2.tar.gz
cd capnproto-c++-0.5.1.2
./configure --enable-shared CXXFLAGS=-fPIC
make -j6 check
sudo make install
rm -rf /capnproto-c++-0.5.1.2

curl -o Mash-1.1.tar.gz https://codeload.github.com/marbl/Mash/tar.gz/v1.1
tar -zxf Mash-1.1.tar.gz
cd /Mash-1.1
./bootstrap.sh
./configure CXXFLAGS=-fPIC
CPPFLAGS="-fPIC" make
make install
rm -rf /Mash-1.1 /Mash-1.1.tar.gz
