#!/usr/bin/env bash
set -e

apt-get update && apt-get install -y \
    autoconf \
    gsl-bin libgsl0-dev \
    zlib1g-dev

curl -O https://capnproto.org/capnproto-c++-0.5.1.2.tar.gz
tar zxf capnproto-c++-0.5.1.2.tar.gz
cd capnproto-c++-0.5.1.2
./configure --enable-shared CXXFLAGS=-fPIC
make -j6 check
make install
cd ..
rm -rf capnproto-c++-0.5.1.2 capnproto-c++-0.5.1.2.tar.gz

curl -o Mash-1.1.tar.gz https://codeload.github.com/marbl/Mash/tar.gz/v1.1
tar -zxf Mash-1.1.tar.gz
cd Mash-1.1
./bootstrap.sh
./configure CXXFLAGS=-fPIC
CPPFLAGS="-fPIC" make
make install
cd ..
rm -rf Mash-1.1 Mash-1.1.tar.gz
