#!/bin/sh
set -e

git clone https://github.com/sandstorm-io/capnproto.git
cd ./capnproto
git checkout e5962ef2 # Hopefully last working commit
cd ./c++
autoreconf -i
./configure --enable-shared CXXFLAGS=-fPIC
make -j6 check
make install
cd ../..
rm -rf capnproto

apk add --no-cache gsl

git clone https://gitlab.com/cgps/mash-original.git
cd mash-original
git checkout alpine
./bootstrap.sh
./configure CXXFLAGS=-fPIC
CPPFLAGS="-fPIC" make
make install
cd ..
rm -rf mash-original
