const mash = require('./build/Release/addon');

function closestMatchSync(referenceMsh, inputFile) {
  const match = mash.closestMatchSync(referenceMsh, inputFile);
  return {
    referenceIndex: match[0],
    mashDistance: match[1],
    pValue: match[2],
    matchingHashes: match[3],
    allHashes: match[4],
  };
}

function closestMatchAsync(referenceMsh, inputFile) {
  return new Promise((resolve, reject) => {
    mash.closestMatchAsync(referenceMsh, inputFile, (error, match) => {
      if (error) {
        reject(error);
      } else {
        resolve({
          referenceIndex: match[0],
          mashDistance: match[1],
          pValue: match[2],
          matchingHashes: match[3],
          allHashes: match[4],
        })
      }
    });
  });
}

function allMatchesSync(referenceMsh, inputFile) {
  const matches = mash.allMatchesSync(referenceMsh, inputFile);
  return matches.map(
    match => ({
      referenceIndex: match[0],
      mashDistance: match[1],
      pValue: match[2],
      matchingHashes: match[3],
      allHashes: match[4],
    })
  );
}

module.exports = {
  closestMatch: closestMatchAsync,
  closestMatchAsync,
  closestMatchSync,
  allMatchesSync,
};
