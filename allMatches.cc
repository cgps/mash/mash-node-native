#include <iostream>
#include <vector>

#include "allMatches.h"
#include "CustomCommandDistance.h"

using namespace::std;

vector<vector<double>> allMatches(vector<string> filenames) {
  CustomCommandDistance cmd;
  vector<CommandDistance::CompareOutput::PairOutput> outputPairs = cmd.getAllMatches(filenames);

  vector<vector<double>> matches;

  for (uint64_t k = 0; k < cmd.outputPairs.size(); k++)
  {
    vector<double> results;
    results.push_back(k);
    results.push_back(outputPairs[k].distance);
    results.push_back(outputPairs[k].pValue);
    results.push_back(outputPairs[k].numer);
    results.push_back(outputPairs[k].denom);

    matches.push_back(results);
  }

  return matches;
}
