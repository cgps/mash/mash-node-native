
#include <node.h>
#include <nan.h>

#include "async.h"
#include "closestMatch.h"

using namespace::std;
using namespace v8;
using namespace Nan;

class ClosestMatchWorker : public AsyncWorker {
  public:
    ClosestMatchWorker(Callback *callback, vector<string> filenames)
    : AsyncWorker(callback), filenames(filenames) {}

    ~ClosestMatchWorker() {}

    void Execute () {
      match = closestMatch(filenames);
    }

    // We have the results, and we're back in the event loop.
    void HandleOKCallback () {
      Nan::HandleScope scope;

      Local<Array> results = New<Array>();
      results->Set(0, New<Number>(match[0]));
      results->Set(1, New<Number>(match[1]));
      results->Set(2, New<Number>(match[2]));
      results->Set(3, New<Number>(match[3]));
      results->Set(4, New<Number>(match[4]));

      Local<Value> argv[] = {
        Null(),
        results
      };

      callback->Call(2, argv);
    }

  private:
    vector<string> filenames;
    vector<double> match;
 };

 // Asynchronous access to the `closestMatch()` function
 NAN_METHOD(ClosestMatchAsync) {
   vector<string> filenames;
   for (unsigned int i = 0; i < 2; i++) {
     v8::String::Utf8Value v8String(info[i]->ToString());
     string stlString = std::string(*v8String);
     filenames.push_back(stlString);
   }
   Callback *callback = new Callback(info[2].As<Function>());
   AsyncQueueWorker(new ClosestMatchWorker(callback, filenames));
 }
